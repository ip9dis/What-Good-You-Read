// HTTP Portion
var http = require('http');
// Path module
var path = require('path');

var jsonfile = require('jsonfile')

var file = 'exam.json'

// Using the filesystem module
var fs = require('fs');

var Twit = require('twit')
var T = new Twit({
    consumer_key: '',
    consumer_secret: '',
    access_token: '',
    access_token_secret: '',
})

T.get('search/tweets', { q: 'currentlyreading', count: 100 }, gotData);

function gotData(err, data, response) {
    jsonfile.writeFile(file, data, function (err) {
        console.error(err)
    })
}

var server = http.createServer(handleRequest);
server.listen(process.env.PORT || 5000);

console.log('Server started on port 8080');

function handleRequest(req, res) {
  // What did we request?
  var pathname = req.url;

  // If blank let's ask for index.html
  if (pathname == '/') {
    pathname = '/index.html';
  }

  // Ok what's our file extension
  var ext = path.extname(pathname);

  // Map extension to file type
  var typeExt = {
    '.html': 'text/html',
    '.js':   'text/javascript',
    '.css':  'text/css'
  };
  // What is it?  Default to plain text

  var contentType = typeExt[ext] || 'text/plain';

  // User file system module
  fs.readFile(__dirname + pathname,
    // Callback function for reading
    function (err, data) {
      // if there is an error
      if (err) {
        res.writeHead(500);
        return res.end('Error loading ' + pathname);
      }
      // Otherwise, send the data, the contents of the file
      res.writeHead(200,{ 'Content-Type': contentType });
      res.end(data);
    }
  );
}
