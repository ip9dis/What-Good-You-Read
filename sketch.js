var tweets; // Stores the tweets JSON file
var book = []; // Books object
var maxTweets;
var bookImage = []; // Images of books of different colors
var xpos, ypos; // Coordinates variables
var column = []; // Used for tracking places where books can fall
var bookLibrary; // Background image
var currentTweet; // Index of last processed tweet
var tweetsShown = 0; // Counts how many tweets are shown
var overBook = false; // Defines if mouse is over a book icon

// Callback function to save tweets from json file
function getTweets(data) {
	tweets = data;
}

// If window is resized, also resize the canvas element
function windowResized() {
	maxTweets = ceil(windowWidth / 100);
	resizeCanvas(windowWidth, windowHeight);
	for(var i = 0; i < currentTweet; i++) {
		if(book[i].stopped == true) {
			book[i].h =  (height - 90);
		}
	}
}

function preload() {
	bookLibrary = loadImage("images/bookLibrary.jpg");
	bookImage[0] = loadImage("images/blueBook.png");
	bookImage[1] = loadImage("images/goldenBook.png");
	bookImage[2] = loadImage("images/grayBook.png");
	bookImage[3] = loadImage("images/greenBook.png");
	bookImage[4] = loadImage("images/redBook.png");
	bookImage[5] = loadImage("images/yellowBook.png");
}

function setup() {
	loadJSON('exam.json', getTweets);
	
	createCanvas(windowWidth, windowHeight); // Creat the canvas element
	frameRate(40); // Set the frame rate to 40 fps

	maxTweets = ceil(windowWidth / 100);
	currentTweet = 0;
}

function draw () {
    background(bookLibrary); // Draw the background

	if(tweets) {
		showTweets();
	}

    if(tweets) {
		for(var i = 0; i < currentTweet; i++) {
			moveBook(i);
			drawBook(i);
			if(book[i].stopped == true) {
				xpos = book[i].p;
				ypos = book[i].h;
				if((mouseX >= xpos && mouseX <= xpos + 50) && (mouseY >= ypos && mouseY <= ypos + 50)) {
					textStyle(BOLD);
					textFont("Helvetica");
					textSize(16);
					text(book[int(mouseX/100)].text, width/2, height/2, 500, 200);
					fill(255, 255, 255);
					overBook = true;
	            }
			}
		}
    }
}

function showTweets() {
	while(currentTweet < maxTweets) {
		createBookObject(currentTweet, tweets.statuses[currentTweet].text);
		currentTweet += 1; // Increase the 'currentTweet' integer by one each frame
		tweetsShown += 1;
	}
}

function mousePressed() {
	if(overBook && book[int(mouseX/100)].stopped == true) {
		overBook = false;
    	forcedMove(int(mouseX/100));
		book[int(mouseX/100)].text = tweets.statuses[tweetsShown].text;
		book[int(mouseX/100)].color = int(random(0, 6));
    	tweetsShown = tweetsShown + 1;
  	}
}

function createBookObject(id, texter) {
	book.push({ text: texter,
				p: id * 100,
				color: int(random(0, 6)),
				h: 0,
				stopped: false
	});
}

function drawBook(id) {
    image(bookImage[book[id].color], book[id].p, book[id].h); // Draws a book image at the top of the display window
}

function moveBook(id) {
	if(book[id].h < (height - 90)) { // Book keep falling until it is close to the end of the window
		book[id].h += 2;
	}
	else {
		book[id].stopped = true;
	}
}

function forcedMove(id) {
	book[id].h = 0;
	book[id].stopped = false;
}
